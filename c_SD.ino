
////////////////////////// ---------------Sd card setup --------------------
#ifdef microSD

#include <SPI.h>
//#include <SD.h>
#include "SdFat.h"

int SD_write_counter = 0;

//File file;

// File system object.
SdFat sd;

// Log file.
SdFile file;

char filename[13];

byte previous_day = 0;

// SD chip select pin.  Be sure to disable any other SPI devices such as Enet.
//const uint8_t chipSelect = 8;
const uint8_t microSD_CS = 8;

// Adapted from SdInfo example. this states the pin, and that the SPI is shared in this case with the USB shield.
// Also the speed is reduced to 1/4th of 16MHz, and it somehow works.
#define SD_CONFIG SdSpiConfig(microSD_CS, SHARED_SPI, SD_SCK_MHZ(4))

void init_SD() {

  Serial.println(F("Initializing SD card..."));

  // see if the card is present and can be initialized:
  if (!sd.begin(SD_CONFIG)) {
    Serial.println(F("Card failed, or not present"));
    while (1) {}
  }

  new_file();
}

void new_file() {
  now = RTC.now();
  // Limited to use the 8:3 file naming convention
  // see here: https://www.arduino.cc/en/Reference/SDCardNotes
  snprintf(filename, sizeof(filename), "%02d%02d%02d%02d.csv", now.day(), now.month(), now.hour(), now.second());
  Serial.println(filename);

  // Open file with following conditions for fast SD access
  // ref: https://forum.arduino.cc/index.php?topic=49649.msg354701#msg354701
  // O_APPEND ~ append everytime a file is openend instead of overwriting it.
  //file = SD.open(filename, O_CREAT | O_APPEND | O_WRITE);
  if (!file.open(filename, O_CREAT | O_APPEND | O_WRITE)) {
    //  error("file.open");
    //file.close();
  }
}

//------------------------------------------------------------------------------
// Log a data record.
void transfer_buffer_to_SD() {
    char buf[7];

  for (int i = 0; i < buffer_len; i++) {
    snprintf(buf, sizeof(buf),"%d %d",x_buffer[buffer_number_to_SD][i],y_buffer[buffer_number_to_SD][i]);
    file.println(buf);
    //Serial.println(buf);
  }

  file.sync();

//  for (int i = 0; i < buffer_len; i++) {
//    Serial.print(xy_buffer[buffer_number_to_SD][i]);
//    Serial.print(',');
//  }

  //file.close();

  if (now.day() != previous_day) {
    file.close();
    new_file();
    previous_day = now.day();
  }
}
#endif
