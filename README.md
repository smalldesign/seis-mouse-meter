# Seis-mouse-meter

Arduino codes for reading mouse inputs as it is attached to a 'seis-mouse-meter' - a seismometer instrument made out of an optical computer mouse, a cheap laser source and some PVC pipe and fittings.

More details - https://hackaday.io/project/180984-low-cost-seismograph-using-optical-mouse

Components needed
- Arduion Uno
- Arduino Uno USB sheild
- RTC module (I2C communication)
- SD card module (SPI communication)

How to?
- Download all the above files in a directory. 
- Open the TnT-Seismousmeter.ino in Arduino IDE. The remaining files should open up as tabs in that IDE.
- Compile and execute.

*** Note - this is work in progress. And updates are sparce. Things are not ready yet for an easy or trouble free implementation!!!
