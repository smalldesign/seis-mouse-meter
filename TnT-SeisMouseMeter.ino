/*  ----------------------------------------------------------------------------------------
    Arduino code to interface and interprete mouse hardware of the seismousemeter
    ----------------------------------------------------------------------------------------
    Seismousmeter of the tube-in-tube type - https://youtu.be/7ok6PN81VBU
    Web-page of project - https://www.notion.so/Experiments-df2cde8e1a4d413fbbcd854721d352eb
    First version of this code 3rd March 2021 by Subir Bhaduri (www.smalldesign.in)
    
    Modifications 4th March
        - Changed filtering method from x-avg to exponential moving average high pass filtering.
        - Added some organization, comments, etc.

    Modifications 9th March
        - SD card logging
        - RTC timestamp (unix timestamp)

    10th March
        - Fix RTC issues?

    15th March, 18th March, 19th March, 20th March
        - Fast SD writing
        - timer triggered
        - Attempting mseed file format
            - shorter 2s frames
            - bigger and descriptive filenames

    6th April
        - Why SD card is not working?
        - Record data only, no time stamp. Timestamp is recorded on filename.

    ----------------------------------------------------------------------------------------
*/

#define  microSD

//-----------------------------------------------------------------------------------------------------
// Add # to disable printing. Remove # to enable.
//#define output

//-----------------------------------------------------------------------------------------------------
// Files for RTC board
#include "RTClib.h"         // RTC library from https://www.elecrow.com/wiki/index.php?title=Tiny_RTC


//-----------------------------------------------------------------------------------------------------
// Include files for USB board
#include <hidboot.h>
#include <usbhub.h>
#include <SPI.h>

USB     Usb;
USBHub     Hub(&Usb);
HIDBoot<USB_HID_PROTOCOL_MOUSE>    HidMouse(&Usb);

//-------------------------------------------------------------
// Variables to hold mouse movements
volatile int8_t dx, dy;       // differential movements from the mouse
volatile int8_t x, y;         // cumulative movements

// from https://stackoverflow.com/questions/43703778/write-data-to-an-sd-card-through-a-buffer-without-a-race-condition
// Also:
const byte buffer_len = 100;                    // 150pts * 0.010 ms = 1.50s of data storage before dumping into SD card.
static volatile int8_t x_buffer[2][buffer_len];
static volatile int8_t y_buffer[2][buffer_len];
//static const int buffer_rows        = sizeof(xy_buffer[0]);
//static const int buffer_char_len    = sizeof(xy_buffer[0]);
volatile byte buffer_count = 0;
//volatile bool flag_buffer_to_SD = false;

// Flag to inform if new measurements are available.
volatile bool movement_flag = false;

// Filter variables for removing mouse drift. (high pass filter).
// from: https://www.norwegiancreations.com/2016/03/arduino-tutorial-simple-high-pass-band-pass-and-band-stop-filtering/
const float EMA_a = 0.3;      //initialization of EMA alpha. Lower is better sensitivity but also has more drift.
volatile float EMA_S_x = 0;        //initialization of EMA S
volatile float EMA_S_y = 0;        //initialization of EMA S


//-----------------------------------------------------------------------------------------------------
// Timing variables
unsigned long lasttime = millis();
const byte sampling_period_ms = 10;
//volatile double old_unixtimestamp = 0;
//volatile byte time_diff = 0;
volatile bool flag_buffer_ready = false;
volatile byte buffer_number_to_SD = 0;
volatile byte buffer_number = 0;

//-----------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------
RTC_DS1307 RTC;
//volatile unsigned long last_update_time;
//volatile byte old_second = 0;
//volatile uint16_t mS = 0;

//volatile double unixtimestamp = 0;

volatile DateTime now;

void init_RTC() {
  RTC.begin();
  //delay(1000);
  //RTC.adjust(DateTime(__DATE__, __TIME__));
  //delay(1000);
  if (! RTC.isrunning()) {
    Serial.println(F("RTC is NOT running!"));
    // following line sets the RTC to the date & time this sketch was compiled
    RTC.adjust(DateTime(__DATE__, __TIME__));
  }
}

// Update the time variables used by all other functions
//void update_time() {
//  now = RTC.now();
//
//  mS = mS + millis() - last_update_time;
//  if (now.second() != old_second) {
//    mS = 0;
//    old_second = now.second();
//  }
//  last_update_time = millis();
//}



//-----------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------
class MouseRptParser : public MouseReportParser
{
  protected:
    void OnMouseMove    (MOUSEINFO *mi);
};

void MouseRptParser::OnMouseMove(MOUSEINFO *mi)
{
  dx = int(mi->dX);
  dy = int(mi->dY);
  movement_flag = true;
};

MouseRptParser Prs;

void init_mouse() {

  if (Usb.Init() == -1) Serial.println(F("OSC did not start."));

  delay( 200 );

  HidMouse.SetReportParser(0, &Prs);
}

//-----------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------
void setup()
{
  Serial.begin( 115200 );

  init_SD();

  init_RTC();

  init_mouse();

    delay(1000);
  init_auto_sampling();

}

//-----------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------
void loop()
{


  if (flag_buffer_ready == true) {
    transfer_buffer_to_SD();
    flag_buffer_ready = false ;
    Serial.println(F("Transferring"));
    //Serial.print(F(','));
    //Serial.println(millis() - lasttime);
  }

  //if (millis() - lasttime > sampling_period_ms) {
  // Update mouse movements if any
  Usb.Task();
  //    lasttime = millis();
  //  }

}
